# Canvas OAuth2 Workflow #

This project is a companion to the following Canvas Community blog post:


- **[.NET - OAuth2 Workflow: Part 1](https://community.canvaslms.com/groups/canvas-developers/blog/2017/04/02/net-oauth2-example)**


### How do I get set up? ###
Tools that you will need:

* Visual Studio - Community Edition should be fine
* IIS or other web server - including an SSL certificate
* Admin access to an instance of Canvas
* Access to a database

Additional info to come as project progresses...